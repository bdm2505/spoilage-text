from .word import Word, PunctuationMark, Symbol
from .analyzer import Analyzer

analyzer = Analyzer()


def parse_text(text):
    text = ' ' + text + ' '
    result = []
    word = ""
    punctuation = ',.!?;'

    for i in range(len(text)):
        ch = text[i]
        if ch.isalpha() or ch in '-':
            word += ch
        else:
            if word == '-':
                result.append(Symbol(word))
            elif word:
                result.append(Word(word, analyzer))
            word = ''
            if ch == ' ':
                continue
            if ch in punctuation:
                result.append(PunctuationMark(ch))
            elif (text[i - 1] != ' ' or i - 1 == 0) and text[i + 1] != ' ':
                result.append(Symbol(ch, False, False))
            elif text[i - 1] != ' ' or i - 1 == 0:
                result.append(Symbol(ch, left_space=False))
            elif text[i + 1] != ' ':
                result.append(Symbol(ch, right_space=False))
            else:
                result.append(Symbol(ch))

    return result


def converse_parse_text(list_word) -> str:
    result = ''
    space = False
    for s in list_word:
        if isinstance(s, Symbol):
            if s.is_empty():
                continue
            elif s.left_space:
                result += ' '
            result += str(s)
            space = s.right_space
        elif isinstance(s, Word):
            if space:
                result += ' '
            space = True
            result += str(s)

    return result
