class Symbol:
    def __init__(self, symbol, left_space=True, right_space=True):
        self.symbol = symbol
        self.left_space = left_space
        self.right_space = right_space

    def __str__(self):
        return self.symbol

    def delete(self):
        self.symbol = ''

    def is_empty(self):
        return not self.symbol


class Word:
    def __init__(self, word, analyzer):
        self.analyzer = analyzer
        self.title = word[0].istitle()
        self.s = word.lower()
        self.prefixes = []
        self.roots = []
        self.suffixes = []
        self.endings = []
        self.postfixes = []
        self.correct = True  # если нет разбора или разбор не совпадает со словом, то False
        self.part_of_speech = None

        self._analyze()

    def _analyze(self):
        self.analyzer.analyze(self)
        
    def __str__(self):
        if self.title:
            return self.s.capitalize()
        return self.s

    def __len__(self):
        return len(self.s)

    def __eq__(self, other):
        if isinstance(other, Word):
            return self.s == other.s
        else:
            return False

    def morphemes(self) -> (list, list, list, list):
        return self.prefixes, self.roots, self.suffixes, self.endings, self.postfixes

    def replace(self, s1, s2):
        self.s = self.s.replace(s1, s2)
        self.correct = False

    def replace_symbol(self, index, s):
        self.s = self.s[:index] + s + self.s[index + 1:]

    def is_correct(self):
        return self.correct

    def build(self):
        self.s = "".join(self.prefixes) + "".join(self.roots) + "".join(self.suffixes) + "".join(self.endings) + "".join(self.postfixes)

    def replace_morphemes(self, fun, list_str):
        if self.correct:
            for i in range(0, len(list_str)):
                list_str[i] = fun(list_str[i])
            self.build()

    def separate(self, index):
        first = Word(self.s[:index], self.analyzer)
        first.title = self.title
        second = Word(self.s[index:], self.analyzer)

        return first, second

    def join_with(self, word):
        new_word = Word(self.s + word.s, self.analyzer)
        new_word.title = self.title
        return new_word


class PunctuationMark(Symbol):
    def __init__(self, symbol):
        super().__init__(symbol, left_space=False)
