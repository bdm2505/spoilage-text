import sqlite3
import os

import pymorphy2
import queue
import copy

pref_list = ['без', 'бес', 'в', 'во', 'вз', 'взо', 'вс', 'вне', 'внутри', 'воз', 'возо', 'вос', 'все', 'вы', 'до',
             'за', 'из', 'изо', 'ис', 'испод', 'к', 'кое', 'кой', 'меж', 'междо', 'между', 'на', 'над', 'надо',
             'наи', 'не', 'небез', 'небес', 'недо', 'ни', 'низ', 'низо', 'нис', 'о', 'об', 'обо', 'обез', 'обес',
             'около', 'от', 'ото', 'па', 'пере', 'по', 'под', 'пол', 'полу', 'подo', 'поза', 'после', 'пра', 'пре', 'пред',
             'предо', 'преди', 'при', 'про', 'противо', 'раз', 'разо', 'рас', 'роз', 'рос', 'с', 'со', 'сверх',
             'среди', 'су', 'сыз', 'тре', 'у', 'чрез', 'через', 'черес', 'а', 'анти', 'архи', 'би', 'вице',
             'гипер', 'де', 'дез', 'дис', 'им', 'интер', 'ир', 'квази', 'контр', 'макро', 'микро', 'обер', 'пост',
             'пре', 'прото', 'псевдо', 'ре', 'суб', 'супер', 'транс', 'ультра', 'экзо', 'экс', 'экстра']

#suff_list = ['ашк','ишк','ышк', 'ушк','юшк','ек','ик','ок','чик','ец','иц','ечк','ичк','очк','еньк','оньк','енк','инк',
#             'онк','ин','к','ушек','ышек','ак','як','ан','ян','анин','янин','ач','ев','ив','евич','евн','ович','овн','ич',
#             'иничн','ичн','ени','ни','ет','от','еств','ств','есть','ость', 'ост','изм','изн','ик','ник','ин','ист','иц','ниц','их',
#             'к','л','лк','льн','льник','льщик','льщиц','тель','итель','ун','чик','щик','чиц','ал','ел','аст','ат','ев','ов',
#             'еват','оват','енн','онн','енск','инск','еньк','оньк','ехоньк','оханьк','ешеньк','ошеньк','ив','ий','ильн','ин',
#             'инск','ист','ит','овит','к','л','лив','н','шн','ск','тельн','уч','юч','яч','чат','чив','янист','а','я','ка','е',
#             'и','нича','ну','ова','ева','ива','ыва','ствова','ть','ене','ени','л','и','ите','сь','ся','ущ','ющ','ащ','ящ','вш',
#             'ш','ем','им','нн','ен','т','в','вши','ши','а','о','жды','учи','ючи','то','либо','нибудь']
# Экспериментальное разделение суффиксов по частям речи
suff_list = []
# for NOUN
noun_suff = ['ашк','ишк','ышк', 'ушк','юшк','ек','ик','ок','чик','ец','иц','ечк','ичк',
             'очк','еньк','оньк','енк','инк','онк','ин','к','ушек','ышек','ак','як',
             'ан','ян','анин','янин','ач','ев','ив','евич','евн','ович','овн','ич',
             'иничн','ичн','ени','ни','ет','от','еств','ств','есть','ость', 'ост',
             'изм','изн','ик','ник','ин','ист','иц','ниц','их','к','л','лк','льн',
             'льник','льщик','льщиц','тель','итель','ун','чик','чик','щик']
# for ADJF ADJS COMP NUMR 
adj_suff = ['ал','ел','ан','ян','аст','ат','ев','ов','еват','оват','енн','онн','ен','он','енск',
            'инск','еньк','оньк','ехоньк','оханьк','ешеньк','ошеньк','ив','ий','ильн',
            'ин','инск','ист','ит','овит','к','л','лив','н','шн','ск','тельн','уч','юч',
            'яч','чат','чив','янист']
# for VERB INFN
verb_suff = ['а','я','ка','е','и','нича','ну','ова','ева','ива','ыва','ствова','ть', 'ти', 'ене','ени','л','и']
# for PRTF PRTS
participle_suff = ['ущ','ющ','ащ','ящ','вш','ш','ем','им','нн','енн','т']
# for GRND
grnd_suff = ['а','я','в','вши','ши']
#for ADVB
adverb_suff = ['а','о','е','и','жды','учи','ючи','ому','ему']

end_list = ['а', 'я', 'о', 'е', 'ь', 'и', 'ы', 'ая ', 'яя', 'ое', 'ее', 'ый', 'у', 'ев',
            'ю', 'ем', 'ешь', 'ете', 'ет', 'ут', 'ют', 'ал', 'ял', 'ала', 'яла', 'али', 'яли', 'ул', 'ула', 'ули',
            'ам', 'ами', 'ас', 'aм', 'ax', 'ая', 'е', 'её', 'ей', 'ем', 'еми', 'емя', 'ex', 'ею', 'ёт', 'ёте',
            'ёх', 'ёшь', 'и', 'ие', 'ий', 'им ', 'ими', 'ит', 'ите','ить', 'их', 'ишь', 'ию', 'м', 'ми', 'мя', 'о', 'ов',
            'ого', 'ое', 'оё', 'ой', 'ом', 'ому', 'ою', 'cм', 'у', 'ум', 'умя', 'ут', 'ух', 'ую', 'шь', 'ями']

# Список частей речи, которые не подвергаются морфемному разбору
unparsed_pos = ['PREP','CONJ','PRCL','INTJ', 'PRED', 'NPRO', None]

# Список частей речи, которые не имеют окончания (даже нулевого)
unchangeable_pos = ['COMP','INFN','GRND','ADVB']

# Части речи, которые могут иметь постфикс -ся, -сь
# todo: Добавить другие типы постфиксов
# Постфиксы есть смысл обрабатывать отдельно, тк они стоят после окончаний
has_postfix = ['VERB', 'INFN', 'GRND', 'PRTF', 'PRTS', 'ADJF', 'ADJS']
postfix_list = ['ся', 'сь', 'те']
# Полный стписок частей речи в документации pymorphy2: https://pymorphy2.readthedocs.io/en/latest/user/grammemes.html

def clear_template(word):
    return "".join(filter(lambda ch: ch.isalpha() or (ch in ["/", "-"]), word))


def common_prefix_len(str1, str2):
    pref_len = 0
    min_len = min(len(str1), len(str2))
    while pref_len < min_len and str1[pref_len] == str2[pref_len]:
        pref_len += 1

    return pref_len

def bfs_prefix(prefix_part):
    q = queue.Queue()
    q.put({'string': prefix_part, 'prefixes': []})

    best_state = {'string': prefix_part, 'prefixes': []}

    while not q.empty():
        curr_state = q.get()
        
        if len(curr_state['string']) < len(best_state['string']):
            best_state = copy.deepcopy(curr_state)
        # Установим ограничение на количество приставок до двух
        if len(curr_state['prefixes']) < 2:
            for prefix in pref_list:
                if (len(prefix) <= len(curr_state['string'])) and (prefix == curr_state['string'][:len(prefix)]):
                    next_state = copy.deepcopy(curr_state)
                    next_state['string'] = next_state['string'][len(prefix):]
                    next_state['prefixes'].append(prefix)

                    q.put(next_state)
                
    return best_state
	
def bfs_suffix(suffix_part):
    #Алгоритм поиска в ширину для поиска наибольшего покрытия строки суффиксами
    #Возвращает словарь state с ключами string and suffixes
    q = queue.Queue()
    q.put({'string': suffix_part, 'suffixes': []})

    best_state = {'string': suffix_part, 'suffixes': []}

    while not q.empty():
        curr_state = q.get()

        if len(curr_state['string']) < len(best_state['string']):
            best_state = copy.deepcopy(curr_state)
	    
        for suffix in suff_list:
            if (len(suffix) <= len(curr_state['string'])) and (suffix == curr_state['string'][-len(suffix):]):
                next_state = copy.deepcopy(curr_state)
                next_state['string'] = next_state['string'][:-len(suffix)]
                next_state['suffixes'].append(suffix)
                
                q.put(next_state)

    best_state['suffixes'].reverse()
    return best_state

def remove_empty_morphs(morphemes):
    #Удалить все пустые морфемы. На данный момент удаляется и нулевое окончание
    return list(filter((lambda morph: morph != ''), morphemes))

    
dir_name = os.path.dirname(__file__)+'/'


class Analyzer:
    def __init__(self):
        self.dictionary = {}
        self.load_db()
        self.pymorphy_analyzer = pymorphy2.MorphAnalyzer()

    def load_db(self):

        con = sqlite3.connect(dir_name + "dictionary.db")
        cur = con.cursor()
        """
        Достанем из базы данных словарь
        Структура таблицы: id, cleanWord, wordParts
        """
        cur.execute("""
            SELECT * FROM Words;
            """)

        result = cur.fetchall()

        for item in result:
            self.dictionary[item[1]] = item[2]

        cur.close()
        con.close()

    def prepare_word(self, analyzed_word):
        """
            Нормализация слова сохраняется в normal_word
            Кроме того, в parsed_word доступна информация о морфологических особенностях слова
        """
        word = str(analyzed_word)
        word = clear_template(word)
        word = word.lower()

        parsed_word = self.pymorphy_analyzer.parse(word)[0]
        normal_word = parsed_word.normal_form
        # Избегаем проблем с е-ё
        normal_word = normal_word.replace("ё", "е")

        return word, parsed_word, normal_word

    def process_prefixes(self, morphemes):
        #выделяем все приставки
        _prefixes = []
        for morph in morphemes:
            if morph in pref_list:
                _prefixes.append(morph)
            else:
                break
        return _prefixes

    def process_postfix(self, suffix_part):
        for postfix in postfix_list:
            if suffix_part.endswith(postfix):
                return postfix, True
        return '', False

    def get_endings(self, suffix_part):
        word_endings = []
        for ending in end_list:
            if len(ending) <= len(suffix_part) and ending == suffix_part[-len(ending):]:
                word_endings.append(ending)
        return word_endings

    def process_suffix_part(self, suffix_part, endings):
        min_root_len = len(suffix_part) + 1  # Просто верхняя граница для длины корня
        _ending = ""
        _suffixes = []
        _suff_rest = ""

        for ending in endings:
            state = bfs_suffix(suffix_part[:len(suffix_part)-len(ending)])
            
            if ((len(state['string']) < min_root_len) or
                (len(state['string']) == min_root_len and len(state['suffixes']) < len(_suffixes))):
                min_root_len = len(state['string'])
            
                _ending = ending
                _suffixes = copy.deepcopy(state['suffixes'])
                _suff_rest = state['string']
                

        return _suff_rest, _suffixes, _ending

    def choose_suffixes(self, pos):
        # Отберем суффиксы в зависимости от части речи
        # todo: сделать независимые списки
        global suff_list
        if pos == "NOUN":
            suff_list = noun_suff + adj_suff
        elif pos in ['ADJF', 'ADJS', 'COMP', 'NUMR', 'PRTF', 'PRTS']:
            # Учитывается некоторая схожесть причастий и прилагательных
            # Причастия заимствуют некоторые глагольные суффиксы
            suff_list = adj_suff + participle_suff + verb_suff
        elif pos in ['VERB', 'INFN']:
            suff_list = verb_suff 
        elif pos == 'GRND':
            suff_list = grnd_suff + verb_suff
        elif pos == 'ADVB':
            suff_list = adverb_suff + adj_suff
	
    # achernin: метод слишком длинный и непонятный. его точно нельзя разбить на несколько?
    def analyze(self, analyzed_word):
        # Возвращает True если слово найдено в словаре
        # Иначе - False
        word, parsed_word, normal_word = self.prepare_word(analyzed_word)

        part_of_speech = parsed_word.tag.POS
        analyzed_word.part_of_speech = part_of_speech

        # FOR DEBUG
        #print(part_of_speech)
        #print(normal_word)
        
        if part_of_speech in unparsed_pos:
            #Unparsed part of speech!
            return True

        self.choose_suffixes(part_of_speech)
        
        dict_word = True
        # Проводим поиск нормализованного слова в словаре
        try:
            divided_word = self.dictionary[normal_word]
            divided_word = divided_word.replace('ё', 'е')
            morphemes = divided_word.split('/')
            morphemes = remove_empty_morphs(morphemes)

            # Отберем только те морфемы, которые находятся в общей части word и normal_word
            pref_len = common_prefix_len(normal_word, word)

            morphs_len = 0
            curr_index = 0
    
            while (curr_index < len(morphemes)) and (morphs_len + len(morphemes[curr_index]) <= pref_len):
                morphs_len += len(morphemes[curr_index])
                curr_index += 1
        
            morphemes = morphemes[:curr_index]

            _prefixes = self.process_prefixes(morphemes)
            morphemes = morphemes[len(_prefixes):]
            analyzed_word.prefixes = _prefixes

            # Выбираем ту часть слова, которая не входит в morphemes
            suffix_part = word[morphs_len:]
            
        except KeyError:
            # todo: добавить обработку "несловарных" слов
            morphemes = []
            suffix_part = word
            
            # Отнимем приставки от слова
            prefix_state = bfs_prefix(suffix_part[:-1])
            suffix_part = prefix_state['string'] + suffix_part[-1]
            analyzed_word.prefixes = prefix_state['prefixes']
            
            dict_word = False
            
        #FOR DEBUG
        #print(morphemes)
        #print(suffix_part)
        
        # Когда постфикс искать не надо => postfix_processed = True
        # 0. Когда он реально найден и process_postfix вернула True
        # 1. Когда часть речи не позволяет
        # 2. Заметим, что если suffix_part не пуста и на её конце нет постфикса, то постфикса нет вообще
        postfix_processed = False
        if part_of_speech in has_postfix:
            _postfix, postfix_processed = self.process_postfix(suffix_part)
            if postfix_processed:
                analyzed_word.postfixes.append(_postfix)
                suffix_part = suffix_part[:-len(_postfix)]
            elif not postfix_processed and len(suffix_part) != 0:
                postfix_processed = True
        else:
            postfix_processed = True

        _suff_rest = ""
        _suffixes = []
        _ending = ""
        
        ending_processed = False
        # Окончание не ищем для неизменяемых слов
        if part_of_speech in unchangeable_pos:
            ending_processed = True
        
        # Обработка пустого suffix_part ведет к появлению нулевого окончания там, где не надо
        if suffix_part != "":
            endings = []
            if not ending_processed:
                endings = self.get_endings(suffix_part)
            # Нулевое окончание является заглушкой и будет удалено для неизменяемых слов
            endings.append('')

            if len(morphemes) == 0:
                # Оставить минимум одну букву на корень
                _suff_rest, _suffixes, _ending = self.process_suffix_part(suffix_part[1:], endings)
                _suff_rest = suffix_part[0] + _suff_rest
            else:
                _suff_rest, _suffixes, _ending = self.process_suffix_part(suffix_part, endings)

            if not ending_processed:
                analyzed_word.endings.append(_ending)
                ending_processed = True
        
            analyzed_word.suffixes = _suffixes + analyzed_word.suffixes

        if len(morphemes) == 0:
            analyzed_word.roots.append(_suff_rest)
            if not ending_processed:
                analyzed_word.endings.append('')
            return dict_word

        # Определим судьбу остатка _suff_rest
        if _suff_rest != "":
            # Если перед _suff_rest стоит корень, то _suff_rest есть смысл отнести к корню (из-за трансформации корней)
            if not morphemes[-1] in (suff_list + end_list + postfix_list):
                morphemes[-1] += _suff_rest
                analyzed_word.roots = morphemes
                morphemes = []
            else:
                # Неопознанная морфема _suff_rest в силу своего расположения может быть отнесена к суффиксам
                analyzed_word.suffixes.insert(0, _suff_rest)
                
        if len(morphemes) == 0:
            if not ending_processed:
                analyzed_word.enings.append('')
            return dict_word
    
        if not postfix_processed and morphemes[-1] in postfix_list:
            postfix_processed = True
            analyzed_word.postfixes.append(morphemes[-1])
            morphemes.pop()

        if not ending_processed:
            if morphemes[-1] in end_list:
                analyzed_word.endings.append(morphemes[-1])
                morphemes.pop()
            else:
                analyzed_word.endings.append('')
            endign_found = True

        while len(morphemes) != 0 and morphemes[-1] in suff_list:
            analyzed_word.suffixes.insert(0, morphemes[-1])
            morphemes.pop()

        analyzed_word.roots = morphemes
        return dict_word
