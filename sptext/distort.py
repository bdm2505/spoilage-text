from . import default
from .parse import parse_text, converse_parse_text
from .word import Word
from .exceptions import SptextException
import random
from .rules import BaseWordRule, BaseTextRule, BasePunctMarkRule

random.seed()


def spoilage(text, rules=default.rules):
    if not text:
        raise SptextException("incorrect text")
    words = parse_text(text)
    rules.sort(key=lambda x: x.priority, reverse=True)

    for rule in rules:
        if isinstance(rule, BaseWordRule) or isinstance(rule, BasePunctMarkRule):
            spoilage_by_word(words, rule)
        elif isinstance(rule, BaseTextRule):
            words = spoilage_by_pattern(words, rule)

    return converse_parse_text(words)


def spoilage_by_word(words, rule):
    for word in words:
        if rule.condition(word) and rule.probability >= random.random():
            rule.distort(word)


def spoilage_by_pattern(words, rule):
    patterns = rule.get_patterns(words)
    end = 0
    result = []
    for (start, size) in patterns:
        if rule.probability >= random.random():
            new_words = rule.distort(words[start:start + size])
        else:
            new_words = words[start:start + size]
        if start > end:
            for word in words[end: start]:
                result.append(word)
        for word in new_words:
            result.append(word)
        end = start + size
    if end < len(words):
        for word in words[end: len(words)]:
            result.append(word)
    return result


