import random

from .word import Word, PunctuationMark

import re

random.seed()


"""
Rule - класс для всех правил

BaseWordRule(Rule) - класс для работы с одним словом
BaseTextRule(Rule) - класс для работы с совокупностью слов
BasePunctMarkRule(Rule) - класс для работы со знаками препинания

BaseCorrectDistort(Rule) - класс, использующий изменение слов только через методы класса word
    - должны выполняться самыми первыми
    
BaseIncorrectDistort(Rule) - класс, использующий иные изменения слов (не сохраняют релевантность разбора)
"""

class Rule:
    def __init__(self, probability=1.0, priority=0):
        self.probability = probability
        self.priority = priority  # чем больше тем раньше будет выполнено правило

    def distort(self, word):
        pass


class BaseWordRule(Rule):
    def condition(self, word) -> bool:
        return isinstance(word, Word) and len(word) > 0


class BaseTextRule(Rule):
    def correct_patterns(self, patterns):
        # Паттерны сортируются так, что у каждого предыдущего конец идет до начала следующего
        # Исключается пересечение и вложение паттернов
        for i in range(len(patterns)):
            for j in range(i + 1, len(patterns)):
                first = patterns[i]
                second = patterns[j]
                if not(first[0] > (second[0] + second[1] - 1) or second[0] > (first[0] + first[1] - 1)):
                    if (first[1] > second[1]):
                        patterns[j] = (-1,0)
                    else:
                        patterns[i] = (-1,0)
                
        patterns = [pattern for pattern in  patterns if pattern != (-1,0)]
        
        patterns.sort(key=lambda p: p[0])
        return patterns

    def get_patterns(self, words):
        # pattern - набор слов(их номера в тексте) list((start, end)), попадающих под правило.
        # Они должны быть изменены вместе.
        # Возвращается список паттернов.
        return []


class BasePunctMarkRule(Rule):
    def condition(self, mark):
        return isinstance(mark, PunctuationMark)


class BaseCorrectDistort(Rule):
    def __init__(self, probability=1.0):
        super().__init__(probability=probability, priority=100)

        
class BaseIncorrectDistort(Rule):
    def __init__(self, probability=1.0):
        super().__init__(probability=probability, priority=1)
        


class RuleSkipPunctuation(BasePunctMarkRule, BaseCorrectDistort):
    def condition(self, mark):
        return isinstance(mark, PunctuationMark)

    def distort(self, mark):
        mark.delete()
        

class SimpleReplace(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, replace_in, replace_out, probability=1.0):
        super().__init__(probability=probability)
        self.replace_in = replace_in
        self.replace_out = replace_out

    def condition(self, word):
        return super().condition(word) and self.replace_in in word.s

    def distort(self, word):
        word.replace(self.replace_in, self.replace_out)


class RuleNN(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("нн", "н", probability)


class RuleZhi(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("жи", "жы", probability)


class RuleShi(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("ши", "шы", probability)


class RuleCha(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("ча", "чя", probability)


class RuleSha(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("ща", "щя", probability)


class RuleChk(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("чк", "чьк", probability)


class RuleChn(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__("чн", "чьн", probability)


class BaseRandomLetter(BaseWordRule, BaseIncorrectDistort):
    def distort(self, word):
        self.rand = random.randint(0, len(word) - 1)
        

class RuleLetterDoubling(BaseRandomLetter):
    def distort(self, word):
        super().distort(word)
        s = word.s
        word.replace_symbol(self.rand, s[self.rand] * 2)


class RuleSkipLetter(BaseRandomLetter):
    def distort(self, word):
        super().distort(word)
        word.replace_symbol(self.rand, '')


similar_letters = {
    'й': 'цыф',
    'ц': 'йфыву',
    'у': 'цывак',
    'к': 'увапе',
    'е': 'капрн',
    'н': 'епрог',
    'г': 'нролш',
    'ш': 'голдщ',
    'щ': 'шлджз',
    'з': 'щджэх',
    'х': 'зжэъ',
    'ъ': 'хэ',
    'ф': 'йцычя',
    'ы': 'йцувсчяф',
    'в': 'цукамсчы',
    'а': 'укепимсв',
    'п': 'кенртима',
    'р': 'енгоьтип',
    'о': 'нгшлбьтр',
    'л': 'гшщдюбьо',
    'д': 'шщзжюбл',
    'ж': 'щзхэюд',
    'э': 'зхъж',
    'я': 'фыч',
    'ч': 'яфывс',
    'с': 'чывам',
    'м': 'свапи',
    'и': 'мапрт',
    'т': 'ипроь',
    'ь': 'тролб',
    'б': 'ьолдю',
    'ю': 'блдж',
}


class RuleMisspell(BaseRandomLetter):
    def distort(self, word):
        super().distort(word)
        sim = similar_letters.get(word.s[self.rand])
        if sim:
            word.replace_symbol(self.rand, random.choice(sim))


class RuleAddExtraLetter(BaseRandomLetter):
    def distort(self, word):
        super().distort(word)
        sim = similar_letters.get(word.s[self.rand])
        if sim:
            word.replace_symbol(self.rand, word.s[self.rand] + random.choice(sim))


class RuleEndingTsya(BaseWordRule, BaseIncorrectDistort):
    def condition(self, word):
        return super().condition(word) and (word.s.endswith('ться') or word.s.endswith('тся'))

    def distort(self, word):
        if word.s.endswith('ться'):
            word.replace('ться', 'тся')
        else:
            word.replace('тся', 'ться')


class RulePrePri(BaseWordRule, BaseIncorrectDistort):
    def condition(self, word):
        return super().condition(word) and (word.s.startswith('пре') or word.s.startswith('при'))

    def distort(self, word):
        if word.s[2] == 'и':
            word.replace_symbol(2, 'e')
        else:
            word.replace_symbol(2, 'и')


class RulePrefixSZ(BaseWordRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def condition(self, word):
        return super().condition(word) and \
               True in map(lambda x: x.endswith('с') or x.endswith('з'), word.prefixes)

    def distort(self, word):
        def fun(s):
            if s.endswith('с'):
                return s[:-1] + 'з'
            else:
                return s[:-1] + 'с'

        word.replace_morphemes(fun, word.prefixes)


class RuleCapitalLetter(BaseWordRule, BaseCorrectDistort):
    def condition(self, word):
        return super().condition(word) and word.title

    def distort(self, word):
        word.title = False


class RuleAdverbEndVowels(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def condition(self, word):
        return (super().condition(word) and (word.part_of_speech == 'ADVB') and
                (word.s.endswith('а') or word.s.endswith('о')))

    def distort(self, word):
        if word.s.endswith('а'):
            word.s = word.s[:len(word.s) - 1] + 'о'
        else:
            word.s = word.s[:len(word.s) - 1] + 'а'


class RuleAdverbEndSoftSign(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.exceptions = ['уж', 'замуж', 'невтерпеж']

    def condition(self, word):
        return (super().condition(word) and (word.part_of_speech == 'ADVB') and
                (word.s.endswith('ь') or (word.s in self.exceptions)))

    def distort(self, word):
        if word.s in self.exceptions:
            word.s += 'ь'
        else:
            word.s = word.s[:len(word.s) - 1]


class RuleEndSoftSignAfterSizzlings(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.sizzlings = ['ж', 'ш', 'ч', 'щ']

    def condition(self, word):
        return (super().condition(word) and
                ((word.s[-1] in self.sizzlings) or (
                    len(word.s) > 1 and word.s[-1] == 'ь' and word.s[-2] in self.sizzlings)))

    def distort(self, word):
        if word.s[-1] == 'ь':
            word.s = word.s[:len(word.s) - 1]
        else:
            word.s = word.s + 'ь'


class RuleSkipSolidSign(SimpleReplace):
    def __init__(self, probability=1.0):
        super().__init__('ъ', '', probability=1.0)


class RuleUnpronounCons(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.exprs = {'вств': 'ств', 'здн': 'зн', 'ндск': 'нск', 'нтск': 'нск',
                      'стл': 'сл', 'стн': 'сн', 'лнц': 'нц', 'рдц': 'рц', 'стск': 'стк', }

    def condition(self, word):
        return (super().condition(word) and
                True in map(lambda expr: expr in word.s, self.exprs.keys()))

    def distort(self, word):
        found_expr = [expr for expr in self.exprs.keys() if expr in word.s][0]  # Замена только одной последовательности
        word.s = word.s.replace(found_expr, self.exprs[found_expr])


class RuleAlternatingVowels(BaseWordRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.alters = {
            'бер': 'бир', 'бир': 'бер',
            'дер': 'дир', 'дир': 'дер',
            'мер': 'мир', 'мир': 'мер',
            'пер': 'пир', 'пир': 'пер',
            'тер': 'тир', 'тир': 'тер',
            'блест': 'блист', 'блист': 'блест',
            'жег': 'жиг', 'жиг': 'жег',
            'стел': 'стил', 'стил': 'стел',
            'чет': 'чит', 'чит': 'чет',
            'лаг': 'лог', 'лож': 'лаж',
            'кас': 'кос', 'кос': 'кас',
            'гар': 'гор', 'гор': 'гар',
            'клан': 'клон', 'клон': 'клан',
            'твар': 'твор', 'твор': 'твар',
            'зар': 'зор', 'зор': 'зар',
            'плав': 'плов', 'плов': 'плав',
            'рос': 'рас', 'рас': 'рос',
            'раст': 'рост', 'рост': 'раст',
            'ращ': 'рощ',
            'скак': 'скок', 'скок': 'скак',
            'скоч': 'скач', 'скач': 'скоч',
            'мак': 'мок', 'мок': 'мак',
            'равн': 'ровн', 'ровн': 'равн',
        }

    def condition(self, word):
        return (super().condition(word) and
                True in map(lambda alter: alter in word.roots, self.alters.keys()))

    def distort(self, word):
        def fun(morpheme):
            if morpheme in self.alters.keys():
                return self.alters[morpheme]
            else:
                return morpheme

        word.replace_morphemes(fun, word.roots)


class RuleNNInSuffixes(BaseWordRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.suffNN = {
            'ан': 'анн', 'анн': 'ан',
            'ян': 'янн', 'янн': 'ян',
            'ин': 'инн', 'инн': 'ин',
            'онн': 'он', 'он': 'онн',
            'енн': 'ен', 'ен': 'енн',
            'н': 'нн', 'нн': 'н',
        }

    def condition(self, word):
        return (super().condition(word) and
                True in map(lambda suff: suff in word.suffixes, self.suffNN.keys()))

    def distort(self, word):
        def fun(morpheme):
            if morpheme in self.suffNN.keys():
                return self.suffNN[morpheme]
            else:
                return morpheme

        word.replace_morphemes(fun, word.suffixes)


class RuleEOAfterSizzlings(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.patternE = re.compile(r'([жшчщ])е')
        self.patternO = re.compile(r'([жшчщ])о')

    def condition(self, word):
        return (super().condition(word) and
                (self.patternE.search(word.s) or self.patternO.search(word.s)))

    def distort(self, word):
        if self.patternE.search(word.s):
            word.s = self.patternE.sub(r'\1о', word.s)
        else:
            word.s = self.patternO.sub(r'\1е', word.s)


class RuleVowelsAfterPrefixes(BaseWordRule, BaseCorrectDistort):  # Гласные ы/и после приставки
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.consonants = 'бвгджзйклмнпрстфхцчшщ'

    def condition(self, word):
        return (super().condition(word) and len(word.prefixes) > 0 and
                word.prefixes[-1][-1] in self.consonants and word.roots[0][0] in ['ы', 'и'])

    def distort(self, word):
        def fun(morpheme):
            if morpheme[0] == 'и':
                return 'ы' + morpheme[1:]
            else:
                return 'и' + morpheme[1:]

        word.replace_morphemes(fun, word.roots)


class RuleVowelsAfterC(BaseWordRule, BaseIncorrectDistort):  # Ы/и после Ц
    def __init__(self, probability=1.0):
        super().__init__(probability)
        self.pattern1 = re.compile('цы')
        self.pattern2 = re.compile('ци')

    def condition(self, word):
        return (super().condition(word) and
                (self.pattern1.search(word.s) or self.pattern2.search(word.s)))

    def distort(self, word):
        if self.pattern1.search(word.s):
            word.s = self.pattern1.sub('ци', word.s)
        else:
            word.s = self.pattern2.sub('цы', word.s)



class BaseRuleCombineWords(BaseTextRule, BaseCorrectDistort):
    def __init__(self, sample, rep="", probability=1.0):
        super().__init__(probability)
        from .parse import parse_text
        self.samples = parse_text(sample)
        self.rep = rep

    def get_patterns(self, words):
        size = len(self.samples)
        result = []
        if len(words) >= size:
            start = 0
            while start <= len(words) - size:
                success = True
                for i in range(size):
                    if words[start + i] != self.samples[i]:
                        success = False
                        break
                if success:
                    result.append((start, size))
                    start += size
                else:
                    start += 1
        return self.correct_patterns(result)

    def distort(self, words):
        s = self.rep.join(map(lambda w: w.s, words))
        word = Word(s, words[0].analyzer)
        word.title = words[0].title
        return [word]


class RuleBokOBok(BaseRuleCombineWords):
    def __init__(self, probability=1.0):
        super().__init__("бок о бок", rep="-", probability=probability)


class BaseRuleCombineMany(BaseTextRule, BaseCorrectDistort):
    def __init__(self, samples, rep="", probability=1.0):
        super().__init__(probability)
        self.samples = samples
        self.rep = rep

    def get_patterns(self, words):
        rules = []
        for sample in self.samples:
            rules.append(BaseRuleCombineWords(sample, self.rep, self.probability))

        result = []
        for rule in rules:
            result = result + rule.get_patterns(words)

        return self.correct_patterns(result)

    def distort(self, words):
        s = self.rep.join(map(lambda w: w.s, words))
        word = Word(s, words[0].analyzer)
        word.title = words[0].title
        return [word]

        
class RuleAdverbCombinations(BaseRuleCombineMany):
    def __init__(self, probability):
        adv_comb = [
        'бок о бок',
        'дверь в  дверь',
        'с глазу на глаз',
        'тютелька в тютельку',
        'один на один',
        'час от часу',
        'точка  в точку',
        'с часу на час',
        ]
        super().__init__(adv_comb, "-", probability=probability)


class RuleSeparatePrefix(BaseTextRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def get_patterns(self, words):
        patterns = []
        for i in range(len(words)):
            word = words[i]
            if isinstance(word, Word) and len(word) > 0 and len(word.prefixes) > 0:
                patterns.append((i, 1))
        return patterns

    def distort(self, words):
        word = words[0]
        s_index = sum(map(lambda s: len(s), word.prefixes))
        first, second = word.separate(s_index)
        return [first, second]


class RuleAddPreposition(BaseTextRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def get_patterns(self, words):
        patterns = []
        for i in range(len(words) - 1):
            if (isinstance(words[i], Word) and isinstance(words[i + 1], Word) and
                (words[i].part_of_speech == 'PREP') and (words[i + 1].part_of_speech == 'NOUN')):
                patterns.append((i,2))
        return patterns

    def distort(self, words):
        return [words[0].join_with(words[1])]


class RuleDistortPrefixPol(BaseWordRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def condition(self, word):
        return super().condition(word) and (len(word.prefixes) > 0) and (word.prefixes[0] == 'пол')

    def distort(self, word):
        def fun(morpheme):
            if morpheme[0] == '-':
                return morpheme[1:]
            else:
                return '-' + morpheme

        word.replace_morphemes(fun, word.roots)


class RuleRemoveHyphen(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def condition(self, word):
        return super().condition(word) and ('-' in word.s)

    def distort(self, word):
        word.s = word.s.replace('-', '')


class RuleNeNi(BaseWordRule, BaseIncorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def condition(self, word):
        return super().condition(word) and (word.s in ['не','ни'])

    def distort(self, word):
        if word.s == 'не':
            word.s = 'ни'
        else:
            word.s = 'не'


class RuleNotWithVerbs(BaseTextRule, BaseCorrectDistort):
    def __init__(self, probability=1.0):
        super().__init__(probability)

    def get_patterns(self, words):
        patterns = []
        for i in range(len(words) - 1):
            if (isinstance(words[i], Word) and isinstance(words[i + 1], Word) and
                (words[i].s == 'не') and (words[i + 1].part_of_speech == 'VERB')):
                patterns.append((i,2))
        return patterns

    def distort(self, words):
        return [words[0].join_with(words[1])]
    


