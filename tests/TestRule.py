import unittest

from sptext import spoilage
from sptext.rules import *
from sptext.parse import parse_text, converse_parse_text


class TestRule(unittest.TestCase):
    def test_rule_nn(self):
        rules = [RuleNN(1)]
        text = "Нам поставили новые деревянные окна. Купленная в магазине рыба оказалась испорченной."
        distorted_text = "Нам поставили новые деревяные окна. Купленая в магазине рыба оказалась испорченой."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_rule_zhi(self):
        rules = [RuleZhi(1)]
        text = "Человек создаёт животным хорошие условия для жизни."
        distorted_text = "Человек создаёт жывотным хорошие условия для жызни."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_rule_shi(self):
        rules = [RuleShi(1)]
        text = "Черепаха всех смешит, потому что не спешит. Но куда спешить тому, кто всегда в своём дому?"
        distorted_text = "Черепаха всех смешыт, потому что не спешыт. Но куда спешыть тому, кто всегда в своём дому?"
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_incorrect_text(self):
        text = "Incorrect text"
        result_text = "Incorrect text"
        self.assertEquals(spoilage(text), result_text)

    def test_None(self):
        text = None
        self.assertRaises(Exception, spoilage, text)

    def test_parse(self):
        text = "'Хороший (или нет) текст', я съел пол-лимона."
        result = ["'", 'Хороший', '(', 'или', 'нет', ')', 'текст', "'", ',', 'я', 'съел', 'пол-лимона', '.']
        self.assertEqual(list(map(str, parse_text(text))), result)

    def test_converse_parse(self):
        text = "'Хороший (или нет) текст', я съел пол-лимона."
        list_word = parse_text(text)
        self.assertEquals(converse_parse_text(list_word), text)

    def test_rule_skip_punctuation(self):
        rules = [RuleSkipPunctuation(1)]
        text = 'В этом 2+3=4 тексте ! много ; знаков, как-то так.'
        result = 'В этом 2+3=4 тексте много знаков как-то так'
        self.assertEqual(spoilage(text, rules), result)

    def test_rule_letter_doubling(self):
        rules = [RuleLetterDoubling(1)]
        text = 'лес а!'
        res = spoilage(text, rules)
        self.assertTrue(res == 'ллес аа!' or res == 'леес аа!' or res == 'лесс аа!')

    def test_rule_skip_letter(self):
        rules = [RuleSkipLetter(1)]
        text = 'лес а!'
        res = spoilage(text, rules)
        self.assertTrue(res == 'ес !' or res == 'лс !' or res == 'ле !')

    def test_rule_misspell(self):
        rules = [RuleMisspell(1)]
        text = 'и'
        res = spoilage(text, rules)
        self.assertTrue(res == 'м' or res == 'а' or res == 'п' or res == 'р' or res == 'т')

    def test_rule_add_extra_letter(self):
        rules = [RuleAddExtraLetter(1)]
        text = 'т'
        res = spoilage(text, rules)
        self.assertTrue(res == 'ти' or res == 'тп' or res == 'тр' or res == 'то' or res == 'ть')

    def test_rule_ending_tsya(self):
        rules = [RuleEndingTsya(1)]
        text = 'Он пошел купаться. ' \
               'Я начала весело смеяться. ' \
               'Эта книга плохо читается. ' \
               'Она смогла спрятаться от меня.'
        result = 'Он пошел купатся. ' \
                 'Я начала весело смеятся. ' \
                 'Эта книга плохо читаеться. ' \
                 'Она смогла спрятатся от меня.'
        self.assertEqual(spoilage(text, rules), result)

    def test_rule_pre_pri(self):
        rules = [RulePrePri(1)]
        text = "Прекрасный день стоял на улице города Москвы. " \
               "Приезжие часто осматривают прилежащие к Красной площади достопримечательности и " \
               "прилежно фотографируются с ними, чтобы потом осталась память..."
        result = "Прикрасный день стоял на улице города Москвы. " \
                 "Прeезжие часто осматривают прeлежащие к Красной площади достопримечательности и " \
                 "прeлежно фотографируются с ними, чтобы потом осталась память..."
        self.assertEqual(spoilage(text, rules), result)

    def test_default_rules(self):
        text = 'Строка – это последовательность символов с произвольным доступом. ' \
               'Строки в языке Python невозможно изменить – в этом случае говорят, что это immutable тип. ' \
               'Попытка изменить символ в определенной позиции или подстроку вызовет ошибку: ' \
               'Строки в питоне можно заключать как в одинарные, так и в двойные кавычки, ' \
               'причем кавычки одного типа могут быть произвольно вложены в кавычки другого типа:'
        print(spoilage(text))

    def test_rule_prefix_SZ(self):
        rules = [RulePrefixSZ(1)]
        text = "Рассказ о разбросанных вещах, был рассказан рассказчиком."
        result = "Разсказ о расбросанных вещах, был разсказан разсказчиком."
        self.assertEquals(spoilage(text, rules), result)

    def test_rule_capital_letter(self):
        rules = [RuleCapitalLetter(1)]
        text = "Очень много Заглавных Букв"
        result = "очень много заглавных букв"
        self.assertEquals(spoilage(text, rules), result)

    def test_rule_bok_o_bok(self):
        rules = [RuleBokOBok(1)]
        text = "А он бок о бок, рядом с ней!"
        result = "А он бок-о-бок, рядом с ней!"
        self.assertEquals(spoilage(text, rules), result)

    def test_cha(self):
        rules = [RuleCha(1)]
        text = "Горячая чашка чая."
        distorted_text = "Горячяя чяшка чяя."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_sha(self):
        rules = [RuleSha(1)]
        text = "Прощай, щавель!"
        distorted_text = "Прощяй, щявель!"
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_chk(self):
        rules = [RuleChk(1)]
        text = "Значки на курточке."
        distorted_text = "Значьки на курточьке."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_chn(self):
        rules = [RuleChn(1)]
        text = "Конечно, он срочно зайдет в прачечную."
        distorted_text = "Конечьно, он срочьно зайдет в прачечьную."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_adverb_end_vowels(self):
        rules = [RuleAdverbEndVowels(1)]
        text = "Справа прошло много, а слева мало."
        distorted_text = "Справо прошло многа, а слево мала."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_adverb_end_soft_sign(self):
        rules = [RuleAdverbEndSoftSign(1)]
        text = "Невтерпеж замуж и вскачь прочь из окна, открытого сильно настежь."
        distorted_text = "Невтерпежь замужь и вскач проч из окна, открытого сильно настеж."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_end_soft_sign_after_sizzlings(self):
        rules = [RuleEndSoftSignAfterSizzlings(1)]
        text = "Невтерпеж замуж и вскачь прочь из окна, открытого сильно настежь. На небе много туч. Нужно беречь природу."
        distorted_text = "Невтерпежь замужь и вскач проч из окна, открытого сильно настеж. На небе много тучь. Нужно береч природу."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_skip_solid_sign(self):
        rules = [RuleSkipSolidSign(1)]
        text = "Адъютант подъехал на трехъярусной карете."
        distorted_text = "Адютант подехал на трехярусной карете."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_unpronoun_cons(self):
        rules = [RuleUnpronounCons(1)]
        text = "Здравствуйте, солнце светит, и сердце радуется."
        distorted_text = "Здраствуйте, сонце светит, и серце радуется."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_alternating_vowels(self):
        rules = [RuleAlternatingVowels(1)]
        text = "Мириться, выдирать, запирать, протирать."
        distorted_text = "Мериться, выдерать, заперать, протерать."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_nn_in_suffixes(self):
        rules = [RuleNNInSuffixes(1)]
        text = "Определенная неопределенность, найдена. Безветренная погода. Звериный инстинкт."
        distorted_text = "Определеная неопределеность, найденна. Безветреная погода. Зверинный инстинкт."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_eo_after_sizzlings(self):
        rules = [RuleEOAfterSizzlings(1)]
        text = "Шорох щелочь крыжовник желтый."
        distorted_text = "Шерох щолочь крыжевник жолтый."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_vowels_after_prefixes(self):
        rules = [RuleVowelsAfterPrefixes(1)]
        text = "Сверхинтересный межинститутский подыграть розыск."
        distorted_text = "Сверхынтересный межынститутский подиграть розиск."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_vowels_after_c(self):
        rules = [RuleVowelsAfterC(1)]
        text = "Цыган цыпят отдал в цирк на репетицию."
        distorted_text = "Циган ципят отдал в цырк на репетицыю."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_separate_prefix(self):
        rules = [RuleSeparatePrefix(1)]
        text = "Придержать, прибежать, наказание, набор."
        distorted_text = "При держать, при бежать, наказание, на бор."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_adverb_combinations(self):
        rules = [RuleAdverbCombinations(1)]
        text = "Бок о бок час от часу не легче. Говорим с глазу на глаз один на один."
        distorted_text = "Бок-о-бок час-от-часу не легче. Говорим с-глазу-на-глаз один-на-один."
        self.assertEquals(spoilage(text, rules), distorted_text)
    
    def test_bok_o_bok(self):
        rules = [RuleBokOBok(1)]
        text = "Бок о бок, бок о бок бок о бок."
        distorted_text = "Бок-о-бок, бок-о-бок бок-о-бок."
        self.assertEquals(spoilage(text, rules), distorted_text)
    
    def test_add_preposition(self):
        rules = [RuleAddPreposition(1)]
        text = "В сердцах, в обрез в хорошем настроении."
        distorted_text = "Всердцах, вобрез в хорошем настроении."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_distort_prefix_pol(self):
        rules = [RuleDistortPrefixPol(1)]
        text = "Пол-арбуза, полбулки хлеба, пол-литра молока или половина лимона."
        distorted_text = "Поларбуза, пол-булки хлеба, поллитра молока или половина лимона."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_remove_hyphen(self):
        rules = [RuleRemoveHyphen(1)]
        text = "Пол-литра темно-красного вишневого сока."
        distorted_text = "Поллитра темнокрасного вишневого сока."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_ne_ni(self):
        rules = [RuleNeNi(1)]
        text = "Ни один не должен ошибаться в этом правиле."
        distorted_text = "Не один ни должен ошибаться в этом правиле."
        self.assertEquals(spoilage(text, rules), distorted_text)

    def test_not_wiht_verbs(self):
        rules = [RuleNotWithVerbs(1)]
        text = "Он не пришел на работу."
        distorted_text = "Он непришел на работу."
        self.assertEquals(spoilage(text, rules), distorted_text)
    
    
if __name__ == '__main__':
    unittest.main()
